module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
      },
      build: {
        src: 'src/js/*.js',
        dest: 'build/js/<%= pkg.name %>.min.js'
      }
    },
    concat: {
      options: {
          separator: '\n;\n'
      },
      dev: {
        src: ['<%= uglify.build.src %>'],
        dest: '<%= uglify.build.dest %>'
      }
    },
    copy: {
      prod: {
        files: [
          {expand: true, cwd: 'src', src: ['libs/**'], dest: 'build/js/'},
          {expand: true, src: ['assets/levels/**'], dest: 'build/'},
          {expand: true, src: ['assets/fonts/**'], dest: 'build/'},
          {expand: true, cwd: 'src', src: ['*'], dest: 'build/', filter: 'isFile'}
        ]
      }
    },
    qunit: {
      files: ['test/*.html']
    },
    jshint: {
      files: ['src/js/*.js'],
      options: {
        globals: {

        }
      }
    },
    watch: {
      files: ['<%= jshint.files %>', '<%= qunit.files %>'],
      tasks: ['jshint', 'qunit']
    }
  });

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-qunit');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-watch');

  // Default task(s).
  grunt.registerTask('default', ['concat:dev', 'copy']);
  grunt.registerTask('prod', ['uglify', 'copy'])
  grunt.registerTask('test', ['jshint', 'qunit']);

};