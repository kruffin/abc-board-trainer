var ABC_GAME = ABC_GAME || {};

ABC_GAME.Util = function () {};

ABC_GAME.Util.prototype = {
	opentypeToTwoPath: function (otPath) {
		var twoPath = [];

		_.each(otPath.commands, function (v) {
			twoPath.push(new Two.Anchor( 
				v.x,
				v.y,
				v.x1,
				v.y1,
				v.x2,
				v.y2,
				v.type
			));
		});

		return twoPath;
	}
};