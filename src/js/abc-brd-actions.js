var ABC_BRD = ABC_BRD || {};

/**
* This variable contains a complete list of the available actions (leetters, 
* numbers, and questions) and how to navigate to it via the ABC board. Looking
* at an example:
* If a one wished to communicate the letter 'A' then one would:
* - look right (index 0 in the ['right', 'left'] keys)
* - say yes to number 1 (index 0 in the [1, 2, 3, 4] keys)
* - say yes to 'A' (index 0 in the ['A', 'B', 'C', 'D'] keys)
* This means we took indices 0, 0, 0 to get to the letter 'A' 
*/
ABC_BRD.actions = {
	'A' : [0, 0, 0],
	'B' : [0, 0, 1],
	'C' : [0, 0, 2],
	'D' : [0, 0, 3],
	'E' : [0, 1, 0],
	'F' : [0, 1, 1],
	'G' : [0, 1, 2],
	'H' : [0, 1, 3],
	'I' : [0, 2, 0],
	'J' : [0, 2, 1],
	'K' : [0, 2, 2],
	'L' : [0, 2, 3],
	'M' : [0, 3, 0],
	'N' : [0, 3, 1],
	'O' : [0, 3, 2],
	'P' : [0, 3, 3],
	'Q' : [1, 0, 0],
	'R' : [1, 0, 1],
	'S' : [1, 0, 2],
	'T' : [1, 0, 3],
	'U' : [1, 1, 0],
	'V' : [1, 1, 1],
	'W' : [1, 1, 2],
	'X' : [1, 1, 3],
	'Y' : [1, 2, 0],
	'Z' : [1, 2, 1],
	'WHO' : [1, 2, 2, 0],
	'WHAT' : [1, 2, 2, 1],
	'WHERE' : [1, 2, 2, 2],
	'WHY' : [1, 2, 2, 3],
	'HOW' : [1, 2, 2, 4],
	'0' : [1, 2, 3, 0],
	'1' : [1, 2, 3, 1],
	'2' : [1, 2, 3, 2],
	'3' : [1, 2, 3, 3],
	'4' : [1, 2, 3, 4],
	'5' : [1, 2, 3, 5],
	'6' : [1, 2, 3, 6],
	'7' : [1, 2, 3, 7],
	'8' : [1, 2, 3, 8],
	'9' : [1, 2, 3, 9]
};