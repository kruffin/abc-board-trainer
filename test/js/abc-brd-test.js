
QUnit.test ("board existence", function (assert) {
	assert.ok(ABC_BRD.board, "Board exists.");
});

QUnit.test ("actions existence", function (assert) {
	assert.ok(ABC_BRD.actions, "Actions exist.");
});


QUnit.test( "board traversal", function( assert ) {
	var key, act, index,
		elem, toMatch;
	for (key in ABC_BRD.actions) {
		elem = undefined;
		if (ABC_BRD.actions.hasOwnProperty(key)) {
			act = ABC_BRD.actions[key];
			if (!act) {
				assert.ok(false, key + " has no action trail.");
			} else {
				for (index = 0; index < act.length; index++) {
					elem = elem || ABC_BRD.board;
					elem = elem[elem.keys[act[index]]];
					if (elem === undefined) {
						assert.ok(false, key + " does not correctly map to the board.");
						elem = undefined;
						break;
					}
				}

				if (elem !== undefined) {
					toMatch = key;
					if (typeof elem === 'string' && key.length > 1) {
						toMatch = key.substring(0, 1).toUpperCase() + key.substring(1).toLowerCase();
					} else if (typeof elem === 'number') {
						toMatch = parseInt(key);
					}
					assert.ok(toMatch === elem, "Key " + key + " maps correctly.");
				}
			}
		}
	}
});