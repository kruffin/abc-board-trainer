var ABC_GAME = ABC_GAME || {};

ABC_GAME.Game = function (elem, height, width) {
	this.params = { width: width, height: height };
	this.two = new Two(this.params).appendTo(elem);
	this.util = new ABC_GAME.Util();
};

ABC_GAME.Game.prototype = {
	start: function () {
		if (!this.level) {
			this.two.bind('update', this.update.bind(this));
			this.level = ABC_GAME.LevelList.intro();
			this.level.setGame(this).init();
		}

		this.two.play();
	},
	pause: function () {
		this.two.pause();
	},
	update: function (frameCount) {
		this.level.update(frameCount);
	}
};