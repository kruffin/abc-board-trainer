var ABC_GAME = ABC_GAME || {};

ABC_GAME.LevelModel = Backbone.Model.extend({urlRoot : '/assets/levels'});


// function (levelFile, game) {
// 	this.game = game;
//};

ABC_GAME.Level = function (key) {
	this._model = new ABC_GAME.LevelModel({ id: key });
};

ABC_GAME.Level.prototype.init = function (cb) {
	var finalFunc = function () {
		this.circle = this._game.two.makeCircle(72, 100, 50);
		this.circle.fill = '#ff8000';
		this.circle.stroke = 'orange';
		this.circle.linewidth = 5;

		var txt = this._game.util.opentypeToTwoPath(this._font.getPath(this._model.get('steps')[0].text, 30, 80, 18));
		this.poly = new Two.Polygon(txt, false, false);
		this.poly.fill = '#80ff00';
		this.poly.stroke = 'green';
		//this.poly.linewidth = 2;
		this._game.two.add(this.poly);
	};

	var fontFunc = function (nextCb, err, font) {
		if (err) {
			console.log('Failed to load the font.');
		} else {
			this._font = font;
			nextCb();
		}
	};

	var tmp = function (nextCb, model, response, options) {
		console.log("level name: " + this._model.get('name') + ", level id: " + this._model.get('id'));

		opentype.load(this._model.get('font'), nextCb);
	};

	this._model.fetch({ success: tmp.bind(this, fontFunc.bind(this, finalFunc.bind(this))), error: tmp.bind(this, fontFunc.bind(this, finalFunc.bind(this))) });
};
ABC_GAME.Level.prototype.update = function (frameCount) {

};
ABC_GAME.Level.prototype.setGame = function (game) {
	this._game = game;
	return this;
};