var ABC_BRD = ABC_BRD || {};

/**
* This is the ABC board representation. It lists out the operations available
* and provides an easily navigatible network. Each object within follows the 
* form:
* { keys: ['a', 'x'], 
* 	'a' : 'a',
* 	'x' : { keys: [], ...}
* }
* In this example the first object has two paths available 'a' and 'x'. 'x' is
* itself another object and likewise has a keys attribute that would define its
* other attributes. This creates a tree structure, where the indices to each 
* child are listed in the keys attribute.
* The keys are useful when we move on to generating a series of yes/no answers
* to recreate what a person communicating a word or phrase would do. 
*/
ABC_BRD.board = {
	keys : ['right', 'left'],
	right: {
		keys : [1, 2, 3, 4],
		1 : {
				keys : ['A', 'B', 'C', 'D'],
				'A' : 'A',
				'B' : 'B',
				'C' : 'C',
				'D' : 'D'
		},
		2 : {
				keys : ['E', 'F', 'G', 'H'],
				'E' : 'E',
				'F' : 'F',
				'G' : 'G',
				'H' : 'H'
		},
		3 : {
				keys : ['I', 'J', 'K', 'L'],
				'I' : 'I',
				'J' : 'J',
				'K' : 'K',
				'L' : 'L'
		},
		4 : {
				keys : ['M', 'N', 'O', 'P'],
				'M' : 'M',
				'N' : 'N',
				'O' : 'O',
				'P' : 'P'
		}
	},
	left: {
		keys : [5, 6, 7],
		5 : {
				keys : ['Q', 'R', 'S', 'T'],
				'Q' : 'Q', 
				'R' : 'R',
				'S' : 'S',
				'T' : 'T'
		},
		6 : {
				keys : ['U', 'V', 'W', 'X'],
				'U' : 'U', 
				'V' : 'V', 
				'W' : 'W',
				'X' : 'X'
		},
		7 : {
				keys : ['Y', 'Z', '?', '#'],
				'Y' : 'Y',
				'Z' : 'Z', 
				'?' : {
					keys : ['WHO', 'WHAT', 'WHERE', 'WHY', 'HOW'],
					'WHO'   : 'Who',
					'WHAT'  : 'What',
					'WHERE' : 'Where',
					'WHY'   : 'Why',
					'HOW'   : 'How'
				}, 
				'#' : {
					keys : ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'],
					'0' : 0,
					'1' : 1,
					'2' : 2,
					'3' : 3,
					'4' : 4,
					'5' : 5,
					'6' : 6,
					'7' : 7,
					'8' : 8,
					'9' : 9
				}
		}
	}
};